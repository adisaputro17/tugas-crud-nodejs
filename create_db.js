var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: ''
});

//Membuka koneksi ke database MySQL
connection.connect(function(err){
    if(err) {
        console.log(err);
    }
});

// Query bisa dilakukan di sini
var sql = "CREATE DATABASE db_nodejs";
connection.query(sql, function(err){
    if(err){
      console.log(err);
    } else {
      console.log("Database created");
    }
});

//Menutup koneksi
connection.end(function(err){
    if(err) {
        console.log(err);
     }
});