var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: "db_nodejs",
});

//Membuka koneksi ke database MySQL
connection.connect(function(err){
    if(err) {
        console.log(err);
    }
});

// Query bisa dilakukan di sini
var sql = `CREATE TABLE customer_bank
(
    cust_id int(11) NOT NULL AUTO_INCREMENT,
    nama varchar(50) NOT NULL,
    alamat varchar(200) NOT NULL,
    kode_pos char(5) NOT NULL,
    no_hp varchar(15) DEFAULT NULL,
    email varchar(50) DEFAULT NULL,
    PRIMARY KEY (cust_id)
)`;

connection.query(sql, function(err){
    if(err){
      console.log(err);
    } else {
      console.log("Table created");
    }
});

//Menutup koneksi
connection.end(function(err){
    if(err) {
        console.log(err);
     }
});