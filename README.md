# Simple CRUD Node.js MySQL

## Install

1. Clone this repository

        git clone https://gitlab.com/adisaputro17/tugas-crud-nodejs.git

2. Install dependencies

        npm install

3. Open `create_db.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: ''

4. Run `create_db.js`

        node create_db.js

5. Open `create_table.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: '',
        database: 'db_nodejs',

6. Run `create_table.js`

        node create_table.js

7. Open `index.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: '',
        database: 'db_nodejs',

8. Run `index.js`

        node index.js
